# import system requirements
import requests
import logging
import json

# import package from parent dir
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import core_utils


if __name__ == '__main__':
  # import config
  cfg=core_utils.read_config()
  
  # Build API string from config
  api_root=core_utils.management_api()
  core_utils.big_text("Status")
  logging.warning("###################################################################################################")
  logging.warning("This CLI will perform a cluster status dump")
  logging.warning("###################################################################################################")

  # make request to endpoint
  r = requests.get(api_root+"/overview")
      
  logging.info("RabbitMQ Status Summary")
  logging.info(core_utils.pretty_print(r.json()))

