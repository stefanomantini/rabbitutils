# import system requirements
import requests
import logging
import json

# import package from parent dir
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import core_utils


if __name__ == '__main__':
  # import config
  cfg=core_utils.read_config()
  
  # Build API string from config
  api_root=core_utils.management_api()
  core_utils.big_text("Queue Status")
  logging.warning("###################################################################################################")
  logging.warning("This CLI will dump queue statistics and metrics")
  logging.warning("###################################################################################################")

  # Get list of created vhosts
  vhosts = core_utils.get_vhosts()
  if vhosts == None:
    logging.error("VHOST API ERROR - script terminating")
    exit()
  else:
    logging.debug("VHOSTS" + core_utils.pretty_print(vhosts))

    # get vhost names (handle url encoding)
    vhost_names = []
    for vh in vhosts:
      if vh["name"]=='/':
        vhost_names.append("%2F")
      else:
        vhost_names.append(vh["name"])

    # ask vhost question
    vhost_answers = core_utils.enquire_vhosts(vhost_names)
    
    # make request to endpoint
    r = requests.get(api_root+"/queues/"+vhost_answers["vhost"])
    if r.status_code==200:
      q_list = []
      logging.debug("API Response successful")
      for q in r.json():
        temp_q_stats = {}
        temp_q_stats["name"]=q["name"]
        temp_q_stats["messages"]=q["messages"]
        temp_q_stats["messages_ready"]=q["messages_ready"]
        temp_q_stats["message_bytes_ram"]=q["message_bytes_ram"]
        temp_q_stats["message_stats_deliver_get"]=q["message_stats"]["deliver_get"]
        temp_q_stats["message_stats_deliver_no_ack"]=q["message_stats"]["deliver_no_ack"]
        temp_q_stats["message_stats_deliver"]=q["message_stats"]["deliver"]
        temp_q_stats["mgr_node"]=q["node"]
        temp_q_stats["consumer_utilisation"]=q["consumer_utilisation"]
        temp_q_stats["gc_max_heap"]=q["garbage_collection"]["max_heap_size"]
        temp_q_stats["gc_min_heap"]=q["garbage_collection"]["min_heap_size"]
        temp_q_stats["gc_fullsweep_after"]=q["garbage_collection"]["fullsweep_after"]
        temp_q_stats["memory"]=q["memory"]
        temp_q_stats["durable"]=q["durable"]
        temp_q_stats["messages_unacknowledged"]=q["messages_unacknowledged"]
        temp_q_stats["messages_unacknowledged_ram"]=q["messages_unacknowledged_ram"]
        temp_q_stats["recoverable_slaves"]=q["recoverable_slaves"]
        temp_q_stats["idle_since"]=q["idle_since"]
        q_list.append(temp_q_stats)
      logging.info(core_utils.pretty_print(q_list))

    else: 
      logging.warning("API request failed :( ")
      logging.error(r.json())
