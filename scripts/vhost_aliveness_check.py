# import system requirements
import requests
import logging
import json

# import package from parent dir
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import core_utils


if __name__ == '__main__':
  # import config
  cfg=core_utils.read_config()
  
  # Build API string from config
  api_root=core_utils.management_api()
  core_utils.big_text("VHost Health")
  logging.warning("###################################################################################################")
  logging.warning("This CLI will create a queue 'aliveness-check', deliver a message to it and consume the message")
  logging.warning("###################################################################################################")

  # Get list of created vhosts
  vhosts = core_utils.get_vhosts()
  if vhosts == None:
    logging.error("VHOST API ERROR - script terminating")
    exit()
  else:
    logging.debug("VHOSTS" + core_utils.pretty_print(vhosts))

    # get vhost names (handle url encoding)
    vhost_names = []
    for vh in vhosts:
      if vh["name"]=='/':
        vhost_names.append("%2F")
      else:
        vhost_names.append(vh["name"])

    # ask vhost question
    vhost_answers = core_utils.enquire_vhosts(vhost_names)
    
    # make request to endpoint
    r = requests.get(api_root+"/aliveness-test/"+vhost_answers["vhost"])
    if r.status_code==200 and r.json()["status"] == "ok":
      logging.info("Queue 'aliveness-test' has been created, written to & consumed from")
      logging.info("VHOST: "+vhost_answers["vhost"]+" IS RESPONSIVE")
    else: 
      logging.warning("VHOST IS NOT HAPPY :( ")
      logging.error(r.json())
